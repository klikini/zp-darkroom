<?php

class ThemeOptions {
	const OPTION_CAROUSEL_HEIGHT = "carousel_height";
	const OPTION_CAROUSEL_WIDTH = "carousel_width";
	const OPTION_ITEM_TYPES = "item_type_display";
	const OPTION_TRANSITION_FADE = "transition_fade";
	const OPTION_FEATURED_ALBUM = "featured_album";
	const OPTION_COLOR_SCHEME = "light_scheme";
	const OPTION_NAVBAR_PLACEMENT = "top_navbar";

	function __construct() {
		foreach ($this->getOptionsSupported() as $option) {
			setThemeOptionDefault($option["key"], $option["default"]);
		}

		$theme = basename(dirname(__FILE__));
		setThemeOption("thumb_size", 350, null, $theme);
		setThemeOption("thumb_crop", true, null, $theme);
		setThemeOption("thumb_crop_height", 350, null, $theme);
		setThemeOption("thumb_crop_width", 350, null, $theme);
	}

	function getOptionsSupported() {
		return [
			_("Carousel height") => [
				"key" => self::OPTION_CAROUSEL_HEIGHT,
				"type" => OPTION_TYPE_TEXT,
				"desc" => _('<p>The height of the featured images carousel in pixels.</p>'
					. '<p>This does not apply to the slidehow page.</p>'),
				"default" => 500
			],
			_("Carousel width") => [
				"key" => self::OPTION_CAROUSEL_WIDTH,
				"type" => OPTION_TYPE_TEXT,
				"desc" => ('<p>The width of the images used in the featured images carousel in pixels.</p>'
					. '<p>The carousel will always fill the width of the window, but the images will be resized to this resolution. A higher value means higher quality, but larger files.</p>'
					. '<p>This does not apply to the slideshow page.</p>'),
				"default" => 2000
			],
			_("Display item types") => [
				"key" => self::OPTION_ITEM_TYPES,
				"type" => OPTION_TYPE_CHECKBOX,
				"desc" => _('Show item types (such as <em>Album</em> or <em>Image</em>) in small text near item titles.'),
				"default" => true
			],
			_("Fade transition") => [
				"key" => self::OPTION_TRANSITION_FADE,
				"type" => OPTION_TYPE_CHECKBOX,
				"desc" => _('<p>Enable this option to fade between images instead of sliding between them.</p>'
					. '<p>This applies to both the homepage carousel and the slidehow page.</p>'),
				"default" => false
			],
			_("Featured album") => [
				"key" => self::OPTION_FEATURED_ALBUM,
				"type" => OPTION_TYPE_TEXT,
				"desc" => _('<p>The name of the <b>dynamic</b> album (for example, with images tagged "Featured") to display at the top of the main page.</p>'
					. '<p>This album will not be listed with the rest in the index, but the images will still be shown in their albums.</p>'
					. '<p>Leave this blank to disable the carousel.</p>'),
				"default" => "Featured.alb"
			],
			_("Light color scheme") => [
				"key" => self::OPTION_COLOR_SCHEME,
				"type" => OPTION_TYPE_CHECKBOX,
				"desc" => _("Enable this option to use light colors instead of dark colors on the site."),
				"default" => false
			],
			_("Top navbar") => [
				"key" => self::OPTION_NAVBAR_PLACEMENT,
				"type" => OPTION_TYPE_CHECKBOX,
				"desc" => _("Show the navbar at the top of the screen instead of the bottom."),
				"default" => false
			],
		];
	}

	function getOptionsDisabled() {
		return [
			"image_gray",
			"thumb_crop",
			"thumb_crop_height",
			"thumb_crop_width",
			"thumb_gray",
			"thumb_size",
		];
	}

	function handleOption($option, $currentValue) {
		switch ($option) {
			default:
				break;
		}
	}
}

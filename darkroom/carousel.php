<?php

function make_carousel(string $id, AlbumBase $album) {
	$images = $album->getImages(0);
	$height = intval(getOption(ThemeOptions::OPTION_CAROUSEL_HEIGHT));
	$width = intval(getOption(ThemeOptions::OPTION_CAROUSEL_WIDTH));

	?>
	<div class="carousel slide <?= getOption(ThemeOptions::OPTION_TRANSITION_FADE) ? "carousel-fade" : null ?> mb-4" id="<?= $id ?>">
		<ol class="carousel-indicators">
			<?php for ($i = 0; $i < count($images); $i++): ?>
				<li data-target="#<?= $id ?>" data-slide-to="<?= $i ?>" class="<?= $i === 0 ? "active" : null ?>"></li>
			<?php endfor; ?>
		</ol>

		<div class="carousel-inner">
			<?php for ($i = 0; $i < count($images); $i++): ?>
				<?php $image = $album->getImage($i); ?>
				<div class="carousel-item <?= $i === 0 ? "active" : null ?>">
					<a href="<?= $image->getLink() ?>">
						<?php $src = $image->getCustomImage($height, null, null, $width, $height, null, null) ?>
						<div class="image d-block" style="height: <?= $height ?>px; background-image: url(<?= $src ?>)"></div>
					</a>

					<div class="carousel-caption">
						<h5><?= $image->getTitle() ?></h5>
					</div>
				</div>
			<?php endfor; ?>
		</div>

		<a class="carousel-control-prev" href="#featured" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#featured" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
	<?php
}
